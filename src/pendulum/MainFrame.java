package pendulum;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

import listeners.MenuListener;
import net.miginfocom.swing.MigLayout;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;

public class MainFrame extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6742897950234196010L;
	
	private MenuListener menuListener = new MenuListener();
	
	public static JMenuItem ustawienia;
	public static Pendulum pendulumPanel;
	public static Settings settingsPanel;
	public static JPanel contentPanel;
	
	public static double h=0.001;
	public static double pendulumM1 = 0.5;
	public static double pendulumM2 = 0.5;
	public static double pendulumL1 = 1;
	public static double pendulumL2 = 1;
	public static double pendulumG = 9.8;
	
	private Thread pendulum;
	private static JTextField m1Field;
	private static JTextField l1Field;
	private static JTextField theta1Field;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_4;
	private JLabel lblNewLabel_5;
	private static JTextField m2Field;
	private static JTextField l2Field;
	private static JTextField theta2Field;
	private static JTextField gField;
	private JLabel lblH;
	private static JTextField hField;
	
	public MainFrame(){
		super("Double Pendulum");
		setResizable(false);
		
		setSize(440*4/3, 640);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		centerFrame();
		makeMenu();
		makeContentPanel();
		makeGraphPanel();
		makePendulumAndSettingsPanel();
		startPendulumThread();
		setVisible(true);
	}
	
	private void centerFrame(){
		// Get the size of the screen
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
				 
		// Determine the new location of the window
		int w = this.getSize().width;
		int h = this.getSize().height;
		int x = (dim.width-w)/2;
		int y = (dim.height-h)/2;
		 
		// Move the window
		this.setLocation(x, y);
	}
	
	private void startPendulumThread(){
		pendulum = new Thread(pendulumPanel);
		pendulum.start();
	}
	
	private void makeContentPanel(){
		contentPanel = new JPanel();
		contentPanel.setVisible(true);
		getContentPane().add(contentPanel);
	}
	
	private void makeGraphPanel(){
	}
	
	private void makePendulumAndSettingsPanel(){
		pendulumPanel = new Pendulum();
		pendulumPanel.setVisible(true);
		settingsPanel = new Settings();
		settingsPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Ustawienia", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		settingsPanel.setVisible(true);
		
		m1Field = new JTextField();
		m1Field.setColumns(10);
		
		l1Field = new JTextField();
		l1Field.setColumns(10);
		
		theta1Field = new JTextField();
		theta1Field.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("M1");
		
		JLabel lblNewLabel_1 = new JLabel("L1");
		
		JLabel lblNewLabel_2 = new JLabel("Theta1");
		
		lblNewLabel_3 = new JLabel("M2");
		
		lblNewLabel_4 = new JLabel("L2");
		
		lblNewLabel_5 = new JLabel("Theta2");
		
		m2Field = new JTextField();
		m2Field.setColumns(10);
		
		l2Field = new JTextField();
		l2Field.setColumns(10);
		
		theta2Field = new JTextField();
		theta2Field.setColumns(10);
		
		JLabel lblG = new JLabel("G");
		
		gField = new JTextField();
		gField.setColumns(10);
		
		JButton submitButton = new JButton("Ustaw");
		submitButton.addActionListener(new MenuListener());
		
		lblH = new JLabel("H");
		
		hField = new JTextField();
		hField.setColumns(10);
		GroupLayout gl_pendulumPanel = new GroupLayout(pendulumPanel);
		gl_pendulumPanel.setHorizontalGroup(
			gl_pendulumPanel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 431, Short.MAX_VALUE)
		);
		gl_pendulumPanel.setVerticalGroup(
			gl_pendulumPanel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 460, Short.MAX_VALUE)
		);
		pendulumPanel.setLayout(gl_pendulumPanel);
		pendulumPanel.setBackground(Color.black);
		contentPanel.setLayout(new MigLayout("", "[570px]", "[460px][120px]"));
		contentPanel.add(settingsPanel, "cell 0 1,alignx center,aligny top");
		settingsPanel.setLayout(new MigLayout("", "[34px][86px][34px][86px][232px]", "[20px][8px][20px][8px][20px][8px][20px]"));
		settingsPanel.add(lblNewLabel, "cell 0 0,alignx left,aligny center");
		settingsPanel.add(lblNewLabel_2, "cell 0 4,alignx left,aligny center");
		settingsPanel.add(lblNewLabel_1, "cell 0 2,alignx left,aligny center");
		settingsPanel.add(theta1Field, "cell 1 4,alignx left,aligny top");
		settingsPanel.add(l1Field, "cell 1 2,alignx left,aligny top");
		settingsPanel.add(m1Field, "cell 1 0,alignx left,aligny top");
		settingsPanel.add(lblG, "cell 0 6,alignx left,aligny center");
		settingsPanel.add(gField, "cell 1 6,alignx left,aligny top");
		settingsPanel.add(lblH, "cell 2 6,alignx left,aligny center");
		settingsPanel.add(hField, "cell 3 6,alignx left,aligny top");
		settingsPanel.add(lblNewLabel_3, "cell 2 0,alignx left,aligny center");
		settingsPanel.add(m2Field, "cell 3 0,alignx left,aligny top");
		settingsPanel.add(lblNewLabel_4, "cell 2 2,alignx left,aligny center");
		settingsPanel.add(l2Field, "cell 3 2,alignx left,aligny top");
		settingsPanel.add(lblNewLabel_5, "cell 2 4,alignx left,aligny center");
		settingsPanel.add(theta2Field, "cell 3 4,alignx left,aligny top");
		settingsPanel.add(submitButton, "cell 4 0 1 7,grow");
		contentPanel.add(pendulumPanel, "cell 0 0,growx,aligny top");
		setStartValues();
	}
	
	private void setStartValues(){
		m1Field.setText(pendulumM1+"");
		m2Field.setText(pendulumM2+"");
		l1Field.setText(pendulumL1+"");
		l2Field.setText(pendulumL1+"");
		theta1Field.setText(Math.toDegrees(Pendulum.zVector.angle1)+"");
		theta2Field.setText(Math.toDegrees(Pendulum.zVector.angle2)+"");
		hField.setText("0.001");
		gField.setText(pendulumG+"");
	}
	
	public static void setValues(){
		String[] values = new String[2];
		values[0]=theta1Field.getText();
		values[1]=theta2Field.getText();
		pendulumPanel.setValues(values);
		pendulumM1=Double.parseDouble(m1Field.getText());
		pendulumM2=Double.parseDouble(m2Field.getText());
		pendulumG=Double.parseDouble(gField.getText());
		h=Double.parseDouble(hField.getText());
		pendulumL1=Double.parseDouble(l1Field.getText());
		pendulumL2=Double.parseDouble(l2Field.getText());
	}
	
	private void makeMenu(){
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Plik");
		ustawienia = new JMenuItem("Ustawienia");
		ustawienia.addActionListener(menuListener);
		menu.add(ustawienia);
		menuBar.add(menu);
		getContentPane().add(menuBar, BorderLayout.NORTH);
	}
}
