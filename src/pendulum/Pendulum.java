package pendulum;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Pendulum extends JPanel implements Runnable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5087734899406447130L;
	double h;
	
	public static Vector zVector = new Vector(Math.PI, Math.PI/2, 0, 0);
	private Vector aVector;
	private Vector bVector;
	private Vector cVector;
	private Vector dVector;
	private Vector z1Vector;
	
	private int[] punkt = new int[2];
	private int[] wahadlo1 = new int[2];
	private int[] wahadlo2 = new int[2];
	private double skala = 100;
	int sX=10;
	private int sY=15;
	private String scaleText = "1 metr";
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.setColor(Color.white);
		g.drawLine(punkt[0], punkt[1], wahadlo1[0], wahadlo1[1]);
		g.drawLine(wahadlo2[0], wahadlo2[1], wahadlo1[0], wahadlo1[1]);
		g.setColor(Color.red);
		g.fillOval(wahadlo1[0]-10, wahadlo1[1]-10, 20, 20);
		g.fillOval(wahadlo2[0]-10, wahadlo2[1]-10, 20, 20);
		
		g.setColor(Color.white);
		g.drawLine(sX, sY, (int) (sX+skala), sY);
		g.drawLine(sX, sY, sX, sY-5);
		g.drawLine(sX+(int)skala, sY, (int)(sX+skala), sY-5);
		g.drawString(scaleText, (int)(sX+skala/3), sY+15);
	}
	
	private void calculateScale(){
		int w = getWidth();
		int h = getHeight();
		int x;
		if(h<w)
			x=h;
		else
			x=w;
		skala=0.45*x/((MainFrame.pendulumL1+MainFrame.pendulumL2));
		
		
	}

	@Override
	public void run() {
		
		while(true){
			calculateScale();
			if(MainFrame.h!=0)
				h=MainFrame.h;
			else
				h=0.001;
			
			aVector = dOmega(zVector);
			bVector = dOmega(zVector.sum(aVector.multi(0.5*h)));
			cVector = dOmega(zVector.sum(bVector.multi(0.5*h)));
			dVector = dOmega(zVector.sum(cVector.multi(h)));
			Vector tempVector = aVector.sum(bVector.multi(2)).sum(cVector.multi(2)).sum(dVector);
			z1Vector = zVector.sum(tempVector.multi(h/6.0));
			
			
			punkt[0]=getWidth()/2; punkt[1]=(int)(0.5*getHeight());
			wahadlo1[0]= (int) (MainFrame.pendulumL1*skala*Math.sin(zVector.angle1)+punkt[0]);
			wahadlo1[1]= (int) (MainFrame.pendulumL1*skala*Math.cos(zVector.angle1))+punkt[1];
			
			wahadlo2[0] = (int) (MainFrame.pendulumL2*skala*Math.sin(zVector.angle2)+wahadlo1[0]);
			wahadlo2[1] = (int) (MainFrame.pendulumL2*skala*Math.cos(zVector.angle2))+wahadlo1[1];
			
			zVector = new Vector(z1Vector);
//			System.out.println(Arrays.toString(wahadlo1));
			
			try {
				Thread.sleep((long)(h*1000));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			this.repaint();
		}
		
		
	}
	
	public void setValues(String[] values){
		zVector.angle1=Math.toRadians(Double.parseDouble(values[0]));
		zVector.angle2=Math.toRadians(Double.parseDouble(values[1]));
		zVector.speed1=0;
		zVector.speed2=0;
	}
	
	private Vector dOmega(Vector v){
		return new Vector(v.speed1, v.speed2, dOmega1(v), dOmega2(v));
	}

	private double dOmega1(Vector v){
		return (-MainFrame.pendulumG*(2*MainFrame.pendulumM1+MainFrame.pendulumM2)*
				Math.sin(v.angle1)-MainFrame.pendulumM2*MainFrame.pendulumG*Math.sin(v.angle1-2*v.angle2)-
				2*Math.sin(v.angle1-v.angle2)*MainFrame.pendulumM2*
				(v.speed2*v.speed2*MainFrame.pendulumL2+v.speed1*v.speed1*MainFrame.pendulumL1*Math.cos(v.angle1-v.angle2)))/
				(MainFrame.pendulumL1*(2*MainFrame.pendulumM1+MainFrame.pendulumM2-MainFrame.pendulumM2*Math.cos(2*v.angle1-2*v.angle2)));
	}
	
	private double dOmega2(Vector v){
		return (2*Math.sin(v.angle1-v.angle2)*(v.speed1*v.speed1*MainFrame.pendulumL1*(MainFrame.pendulumM1+MainFrame.pendulumM2)+
				MainFrame.pendulumG*(MainFrame.pendulumM1+MainFrame.pendulumM2)*Math.cos(v.angle1)+v.speed2*v.speed2*MainFrame.pendulumL2*
				MainFrame.pendulumM2*Math.cos(v.angle1-v.angle2)))/(MainFrame.pendulumL2*(2*MainFrame.pendulumM1+MainFrame.pendulumM2-
				MainFrame.pendulumM2*Math.cos(2*v.angle1-2*v.angle2)));
	}
	
	

}
