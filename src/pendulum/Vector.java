package pendulum;

public class Vector {

	double angle1; double angle2; double speed1; double speed2;
	
	public Vector(double a, double b, double c, double d){
		angle1=a; angle2=b; speed1=c; speed2=d;
	}
	
	public Vector(Vector v){
		angle1=v.angle1; angle2=v.angle2; speed1=v.speed1; speed2=v.speed2;
	}
	
	public Vector sum(Vector v){
		return new Vector(angle1+v.angle1, angle2+v.angle2, speed1+v.speed1, speed2+v.speed2);
	}
	
	public Vector multi(double d){
		return new Vector(angle1*d, angle2*d, speed1*d, speed2*d);
	}

}
